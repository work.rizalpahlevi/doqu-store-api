<?php

namespace App\Serializers;

use League\Fractal\Serializer\ArraySerializer;

class DataSerializer extends ArraySerializer
{
    // public function collection($resourceKey, array $data)
    // {
    //     if ($resourceKey) {
    //         return [$resourceKey => $data];
    //     }

    //     return $data;
    // }

    public function collection(?string $resourceKey, array $data): array
    {
        return [$resourceKey ?: 'data' => $data];
    }

    // public function item($resourceKey, array $data)
    // {
    //     if ($resourceKey) {
    //         return [$resourceKey => $data];
    //     }
    //     return $data;
    // }

    public function item(?string $resourceKey, array $data): array
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }

    // public function null()
    // {
    //     return [];
    // }

    // public function mergeIncludes($transformedData, $includedData)
    // {
    //     $includedData = array_map(function ($include) {
    //         if (isset($include['data'])) {
    //             return $include['data'];
    //         }
    //         return $include;
    //     }, $includedData);

    //     return parent::mergeIncludes($transformedData, $includedData);
    // }
}
