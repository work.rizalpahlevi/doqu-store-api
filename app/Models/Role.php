<?php

namespace App\Models;

use App\Observers\CudObserver;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class Role extends Model 
{
    use HasApiTokens, Authenticatable, HasFactory, SoftDeletes, CudObserver;

    protected $table = 'roles';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'key_name', 'name', 'description', 'status_id', 'created_by', 'modified_by'
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'modified_by');
    }

    public function accessRoles() {
        return $this->hasManyThrough(AccessList::class, AccessListRole::class, 'role_id', 'id', null, 'access_list_id');
    }
}
