<?php

namespace App\Models;

use App\Observers\CudObserver;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Laravel\Passport\HasApiTokens;

class Village extends Model
{
    use HasApiTokens, Authenticatable, Authorizable, HasFactory, SoftDeletes, CudObserver;
    
    protected $table = 'villages';
    protected $dates = ['deleted_at'];

    public function customer()
    {
        return $this->hasMany(MasterCustomer::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'modified_by');
    }
    
    public function deleter()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }

}
