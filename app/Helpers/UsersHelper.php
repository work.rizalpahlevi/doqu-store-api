<?php

namespace App\Helpers;

use App\Models\User;


class UsersHelper
{


    public static function getCurrentUser()
    {
        $user_id = auth()->guard('api')->user()->id;
        $user = User::with('role')->find($user_id);

        return $user;
    }
}
