<?php

namespace App\Helpers;

use Carbon\Carbon;
use DateTime;
use Exception;

class DateHelper
{

    public static function getCurrentTime()
    {
        $currentTimeMillis = Carbon::now('GMT+7')->timestamp * 1000;
        return $currentTimeMillis;
    }

    public static function formatTime($timeMillis, $format = 'Y-m-d H:i:s')
    {
        $carbonInstance = Carbon::createFromTimestamp($timeMillis / 1000);
        $formattedTime = $carbonInstance->format($format);

        return $formattedTime;
    }

    public static function getTimezoneFromTimestampMs($timestampMs)
    {
        // Convert milliseconds to seconds
        $timestampSeconds = $timestampMs / 1000;

        // Create a DateTime object from the timestamp in seconds
        $dateTime = new DateTime("@$timestampSeconds");

        // Get the time zone
        $timezone = $dateTime->getTimezone();

        return $timezone->getName();
    }

    public static function compareDatetimeStrings($datetimeString1, $datetimeString2)
    {
        // dd($datetimeString1, $datetimeString2);
        // Convert datetime strings to DateTime objects
        $dateTime1 = DateTime::createFromFormat('H:i:s', $datetimeString1);
        $dateTime2 = DateTime::createFromFormat('H:i:s', $datetimeString2);

        // Check if conversion is successful
        if ($dateTime1 === false || $dateTime2 === false) {
            throw new Exception('Invalid datetime string format');
        }

        // Compare DateTime objects
        if ($dateTime1 > $dateTime2) {
            return 1;
        } elseif ($dateTime1 < $dateTime2) {
            return 2;
        } else {
            return 0;
        }
    }

    public static function getDifference($time1, $time2)
    {
        // Convert timestamps to Carbon instances
        $carbon1 = Carbon::parse($time1);
        $carbon2 = Carbon::parse($time2);

        // Calculate the difference in minutes
        $minuteDifference = $carbon2->diffInMinutes($carbon1);

        return $minuteDifference;
    }
}
