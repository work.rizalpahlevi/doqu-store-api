<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ConnectionHelper
{
    public static function defaultConnection()
    {
        DB::disconnect('sqlsrv');
        Config::set('database.connections.sqlsrv', [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ]);
        DB::reconnect('sqlsrv');
    }

    public static function migrateConnection($request)
    {
        DB::disconnect('sqlsrv');
        Config::set('database.connections.sqlsrv', [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST'),
            // 'database' => $request->header('name'),
            'database' => $request->database_name,
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ]);
        DB::reconnect('sqlsrv');
    }

    public static function superConnection($db_database = "medhop_saas")
    {
        DB::disconnect('mysql');
        Config::set('database.connections.mysql', [
            'driver' => 'mysql',
            'host' => env('DB_HOST'),
            'database' => $db_database,
            'username' => env('DB_USERNAME_SUPER'),
            'password' => env('DB_PASSWORD_SUPER'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ]);
        DB::reconnect('mysql');
    }
}
