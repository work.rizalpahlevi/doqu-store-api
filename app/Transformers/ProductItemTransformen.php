<?php

namespace App\Transformers;

use App\Models\ProductItem;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProductItemTransformen extends TransformerAbstract
{
    function transform(ProductItem $item)
    {
        return [
            'id' => $item->id,
            'product_id' => $item->product_id,
            'product_name' => $item->productGame->name ?? '',
            'quantity' => $item->quantity,
            'price' => $item->price,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'updated_by' => $item->updated_by,
            'updated_by_name' => $item->updater->name ?? '',
            'deleted_at' => $item->deleted_at,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/log-list/' . $item->id,
                ]
            ],
        ];
    }
}
