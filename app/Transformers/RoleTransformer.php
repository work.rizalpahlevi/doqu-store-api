<?php

namespace App\Transformers;

use App\Models\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    function transform(Role $item)
    {
        $status ="";
        if($item->id == 1 || $item->id == 2){
            $status = "special";
        }else{
            $status = "non_special";
        }
        return [
            'id' => $item->id,
            'key_name' => $item->key_name,
            'name' => $item->name,
            'description' => $item->description,
            'access_roles' => $item->accessRoles,
            'status_id' => $item->status_id,
            'status' => $status,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'modified_by' => $item->modified_by,
            'modified_by_name' => $item->updater->name ?? '',
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/roles/' . $item->id,
                ]
            ],
        ];
    }
}
