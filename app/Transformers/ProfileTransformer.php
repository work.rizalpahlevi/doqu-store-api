<?php

namespace App\Transformers;

use App\Models\Profile;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
    function transform(Profile $item)
    {
        return [
            'id' => $item->id,
            'name' => $item->user->name ?? '',
            'email' => $item->user->email ?? '',
            'phone' => $item->phone,
            'gender' => $item->gender == 0 ? "Laki-laki" : "Perempuan",
            'date_birth' => Carbon::parse($item->date_birth)->format('d-m-Y'),
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'updated_by' => $item->updated_by,
            'updated_by_name' => $item->updater->name ?? '',
            'deleted_at' => $item->deleted_at,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/log-list/' . $item->id,
                ]
            ],
        ];
    }
}
