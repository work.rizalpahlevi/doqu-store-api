<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    function transform(User $item)
    {
        
        return [
            'id' => $item->id,
            'name' => $item->name,
            'email' => $item->email,
            'role_id' => $item->role_id,
            'role_name' => $item->role->name ?? '',
            'role_object_id' => $item->role->object_id ?? '',
            'phone' => $item->phone,
            'mobile_phone' => $item->mobile_phone,
            'description' => $item->description,
            'photo' => env('AWS_PATH') . $item->photo,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'updated_by' => $item->updated_by,
            'updated_by_name' => $item->updater->name ?? '',
            'deleted_at' => $item->deleted_at,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/users/' . $item->id,
                ]
            ],
        ];
    }
}
