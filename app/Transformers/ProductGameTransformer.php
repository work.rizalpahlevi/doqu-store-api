<?php

namespace App\Transformers;

use App\Models\ProductGame;
use League\Fractal\TransformerAbstract;

class ProductGameTransformer extends TransformerAbstract
{
    function transform(ProductGame $item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'icon' => env('APP_ASSETS_URL') . $item->icon,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'updated_by' => $item->updated_by,
            'updated_by_name' => $item->updater->name ?? '',
            'deleted_at' => $item->deleted_at,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/log-list/' . $item->id,
                ]
            ],
        ];
    }
}
