<?php

namespace App\Transformers;

use App\Models\Promotion;
use League\Fractal\TransformerAbstract;

class PromotionTransformer extends TransformerAbstract
{
    function transform(Promotion $item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'banner' => env('APP_ASSETS_URL') . $item->banner,
            'created_at' => $item->created_at,
            'created_by' => $item->created_by,
            'created_by_name' => $item->creator->name ?? '',
            'updated_at' => $item->updated_at,
            'updated_by' => $item->updated_by,
            'updated_by_name' => $item->updater->name ?? '',
            'deleted_at' => $item->deleted_at,
            'links'   => [
                [
                    'rel' => 'self',
                    'uri' => '/api/log-list/' . $item->id,
                ]
            ],
        ];
    }
}
