<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Support\Facades\DB;

class RoleRepository
{
    public function getAll($pageSize, $queryParams)
    {
        $paginator = new Role();
        $paginator = $paginator->orderBy('created_at', 'DESC')->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }

    public function getSelectStaff($pageSize, $queryParams)
    {
        $paginator = new Role();
        $paginator = $paginator->where('key_name', '!=', 'super_admin')->where('key_name', '!=', 'admin');
        $paginator = $paginator->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }

    public function getSelectUser($pageSize, $queryParams)
    {
        $paginator = new Role();
        // $paginator = $paginator->where('key_name', '!=', 'super_admin');
        $paginator = $paginator->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }

    // public function getSelectAdmin($pageSize, $queryParams)
    // {
    //     $paginator = new Role();
    //     $paginator = $paginator->where('key_name', '!=', 'super_admin');
    //     $paginator = $paginator->paginate($pageSize);
    //     $paginator->appends($queryParams);
    //     $items = $paginator->getCollection();
    //     $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
    //     return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    // }

    public function getById($id)
    {
        return Role::find($id);
    }

    public function getDetailRoleMeta($id){
        $metaRole = DB::table('access_list_role')
        ->join('roles', function($join){
            $join->on('access_list_role.role_id', '=', 'roles.id')->whereNull('roles.deleted_at');
        })
        ->join('access_lists', function($join) {
            $join->on('access_list_role.access_list_id', '=', 'access_lists.id')
            ->whereNull('access_lists.deleted_at');
        })
        ->where('access_list_role.role_id', '=', $id)
        ->select('access_lists.code as access_list_code')->pluck('access_list_code')->all();

        $metaStr = implode(", ", $metaRole);
        $access_right = ['access_right' => $metaStr, 'access_right_display' => $metaRole];

        return $access_right;
    }

    public function insert($input)
    {
        $item = new Role();
        $item->key_name = $input->has('key_name') ?  $input['key_name'] : $input['name'];
        $item->name = $input['name'];
        $item->description = $input->has('description') ?  $input['description'] : '';
        $item->status_id = $input->has('status_id') ? $input['status_id'] : 1;
        $item->save();
        return $item;
    }

    public function update($id, $input)
    {
        $item = Role::find($id);
        if (!$item)
            return null;

        $item->key_name = $input->has('key_name') ?  $input['key_name'] : $item->key_name;
        $item->name = $input['name'];
        $item->description = $input->has('description') ?  $input['description'] : $item->description;
        $item->status_id = $input->has('status_id') ? $input['status_id'] : $item->status_id;
        $item->save();
        return $item;
    }

    public function delete($id)
    {
        $item = Role::find($id);
        if (!$item)
            return null;

        $item->delete();
        return $item;
    }
}
