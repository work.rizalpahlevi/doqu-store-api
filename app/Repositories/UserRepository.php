<?php

namespace App\Repositories;

use App\Helpers\StorageHelper;
use App\Imports\UserImport;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\ImageManagerStatic as Image;

class UserRepository
{
    public function getAll($pageSize, $queryParams, $request)
    {
        $paginator = new User();

        if ($request->has('role_id') && $request['role_id'] != '') {
            $paginator = $paginator->where('role_id', '=', $request->role_id);
        }
        if ($request->has('name') && $request['name'] != '') {
            $paginator = $paginator->where('name', 'LIKE', "%$request->name%");
        }

        $paginator = $paginator->orderBy('id', 'DESC')->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        // $paginatorAdapter = new Paginator($items, $pageSize, $queryParams);
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }

    public function getAllUserRole($pageSize, $queryParams, $request)
    {
        $paginator = new User();
        if ($request->has('role_id') && $request['role_id'] != '') {
            $paginator = $paginator->where('role_id', '=', $request->role_id);
        }
        if ($request->has('nama') && $request['nama'] != '') {
            $paginator = $paginator->where('user_nama', 'LIKE', "%$request->name%");
        }

        $paginator = $paginator->orderBy('id', 'DESC')->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        // $paginatorAdapter = new Paginator($items, $pageSize, $queryParams);
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }

    public function me()
    {
        // $user = Auth::user();
        $user_id = auth('api')->user()->id;
        $user = User::find($user_id);
        return $user;
    }

    public function getById($id)
    {
        $user =  User::find($id);
        return $user;
    }

    public function getDetailUserMeta($id)
    {
        $metaUser = DB::table('access_list_user')
            ->join('users', function ($join) {
                $join->on('access_list_user.user_id', '=', 'users.id')->whereNull('users.deleted_at');
            })
            ->join('access_lists', function ($join) {
                $join->on('access_list_user.access_list_id', '=', 'access_lists.id')
                    ->whereNull('access_lists.deleted_at');
            })
            ->where('access_list_user.user_id', '=', $id)
            ->select('access_lists.code as access_list_code')->pluck('access_list_code')->all();

        $metaStr = implode(", ", $metaUser);
        $access_right = ['access_right' => $metaStr, 'access_right_display' => $metaUser];

        return $access_right;
    }

    public function insert($input)
    {

        $item = new User();
        $item->name = $input['name'];
        $item->email = $input['email'];
        $item->role_id = $input['role_id'];
        $item->phone = $input['phone'];
        $item->mobile_phone = $input['phone'];
        $item->description = $input['description'];
        $item->password = Hash::make($input['password']);
        $item->photo = "/users/default-user.svg";

        $item->save();
        return $item;
    }

    public function update($id, $input)
    {
        $item = User::find($id);
        if (!$item)
            return null;

        $filePath = "";

        if ($input->hasFile('photo')) {
            $image = $input->file('photo');
            $imageName = time() . Str::slug($input->name) . '.' . $image->getClientOriginalExtension();

            try {
                // upload image s3
                $filePath = StorageHelper::store("/images/profile/", $imageName, file_get_contents($image->getRealPath()));
            } catch (\Exception $e) {
                return response()->json([
                    'title' => 'warning',
                    'message' => 'Proses upload gambar gagal, harap coba lagi atau hubungi developer',
                    'exception' => $e->getMessage()
                ], 422);
            }
        }

        $item->name = $input['name']   ?? $item->name;
        $item->email = $input['email'] ?? $item->email;
        $item->role_id = $input['role_id'] ?? $item->role_id;
        $item->phone = $input['phone'] ?? $item->phone;
        $item->mobile_phone = $input['phone'] ?? $item->mobile_phone;
        $item->description = $input['description'] ?? $item->description;
        $item->password = Hash::make($input['password']) ?? $item->password;
        $item->photo = $filePath;

        $item->save();
        return $item;
    }

    public function delete($id)
    {
        $item = User::find($id);
        if (!$item)
            return null;

        $item->delete();
        return $item;
    }

    public function setUserOn($id)
    {
        $item = User::find($id);
        if (!$item)
            return null;

        $item->is_active = 1;
        $item->save();
        return $item;
    }

    public function setUserOff($id)
    {
        $item = User::find($id);
        if (!$item)
            return null;

        $item->is_active = 0;
        $item->save();
        return $item;
    }

    private function get3CharsEmailName($email, $name)
    {
        $userName = str_replace(' ', '', $email);
        $folderName = 'default';
        if (strlen($userName) > 3) {
            $folderName = substr($userName, 0, 3);
        } else {
            $folderName = $userName;
        }
        return $folderName . $name;
    }

    public function changePassword($request, $id)
    {
        $item = User::find($id);
        if (!$item) {
            return null;
        }
        $item->password = $request->has('new_password') ? Hash::make($request['new_password']) : $item->password;
        $item->save();
        return $item;
    }

    public function importExcel($request)
    {
        $item = DB::transaction(function () use ($request) {
            $file = $request->file('file');
            if ($file) {
                try {
                    $excel = Excel::import(new UserImport(), $file);
                    //    return response()->json($excel, 200);
                } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                    $failures = $e->failures();
                    $error = [];
                    foreach ($failures as $failure) {
                        $key = $failure->attribute();
                        $value = $failure->errors();
                        $error[$key] = $value;
                    }
                    return response()->json($error, 422);
                }
                // response
                $response = [
                    'title' => 'Berhasil',
                    'message' => 'Berhasil import user'
                ];
                return $response;
            }
        });

        return $item;
    }

    public function reset($input)
    {
        // $item = User::where('resource', $input->resource)->whereHas('role', function(Builder $query) {
        //     $query->whereNotIn('key_name', ['korwil', 'korda', 'konfirmator']);
        // })->get();

        $item = User::where('resource', $input->resource)->whereHas('role', function (Builder $query) {
            $query->whereNotIn('key_name', ['korwil', 'korda', 'konfirmator']);
        })->delete();
        return $item;
    }
}
