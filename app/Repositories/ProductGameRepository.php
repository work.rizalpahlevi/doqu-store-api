<?php

namespace App\Repositories;


use App\Models\ProductGame;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductGameRepository
{
    public function getAll($pageSize, $queryParams, $request)
    {

        $paginator = new ProductGame();

        if($request->name != null){
            $paginator = $paginator->where('name','LIKE','%'. $request->name .'%');
        }

        $paginator = $paginator->paginate($pageSize);
        $paginator->appends($queryParams);
        $items = $paginator->getCollection();
        $paginatorAdapter = new IlluminatePaginatorAdapter($paginator);
        return ['items' => $items, 'paginator_adapter' => $paginatorAdapter];
    }
    
    public function getById($id)
    {
        return ProductGame::find($id);
    }
}
