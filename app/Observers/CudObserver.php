<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;

trait CudObserver
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $user = auth('api')->user();
            $model->created_by = $user->id ?? 0;
            $model->updated_by = $user->id ?? 0;
        });

        static::updating(function ($model) {
            $user = auth('api')->user();
            $model->updated_by = $user->id ?? 0;
        });

        static::deleting(function ($model) {
            // $user = auth('api')->user();
            // // $model->updated_by = $user->id ?? 0;
            // $model->deleted_by = $user->id ?? 9;
            $model->deleted_by = 5;
        });

        static::deleted(function ($model) {
            // $user = auth('api')->user();
            // // $model->updated_by = $user->id ?? 0;
            // $model->deleted_by = $user->id ?? 9;
            $model->deleted_by = 3;
        });

    }
}
