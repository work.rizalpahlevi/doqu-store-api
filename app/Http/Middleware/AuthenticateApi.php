<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;


class AuthenticateApi
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // dd($this->auth->guard($guard)->check());
        // dd($this->auth->guard($guard)->guest());
        if ($this->auth->guard($guard)->guest()) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }

    // public function handle($request, Closure $next, ...$guards)
    // {
    //     $this->authenticate($request, $guards);

    //     return $next($request);
    // }

    // /**
    //  * Determine if the user is logged in to any of the given guards.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  array  $guards
    //  * @return void
    //  *
    //  * @throws \Illuminate\Auth\AuthenticationException
    //  */
    // protected function authenticate($request, array $guards)
    // {
    //     if (empty($guards)) {
    //         $guards = [null];
    //     }

    //     foreach ($guards as $guard) {
    //         if ($this->auth->guard($guard)->check()) {
    //             return $this->auth->shouldUse($guard);
    //         }
    //     }

    //     $this->unauthenticated($request, $guards);
    // }

    // protected function unauthenticated($request, array $guards)
    // {
    //     return response('Unauthorized.', 401);
    // }
}
