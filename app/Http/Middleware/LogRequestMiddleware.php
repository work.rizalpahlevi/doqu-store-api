<?php

namespace App\Http\Middleware;

use App\Helpers\UsersHelper;
use App\Http\Controllers\CurrentStatusController;
use Closure;
use Illuminate\Support\Facades\Log;

class LogRequestMiddleware
{
    public function handle($request, Closure $next)
    {

        $user = UsersHelper::getCurrentUser();
        // Log informasi permintaan
        Log::info('Request:', [
            'method' => $request->method(),
            'path' => $request->path(),
            'query' => $request->query(),
            'user_id' => $user->id, // Jika menggunakan otentikasi
            'user_name' => $user->name, // Jika menggunakan otentikasi
            'role_id' => $user->id, // Jika menggunakan otentikasi
            'role_name' => $user->role->name, // Jika menggunakan otentikasi
        ]);

        // Lanjutkan ke middleware atau controller berikutnya
        return $next($request);
    }
}
