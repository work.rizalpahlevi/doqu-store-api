<?php

namespace App\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class UserRequest extends CustomFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr = [
            'name' => 'required|string|max:255',
            'role_id' => 'required|numeric',
            'description' => 'nullable|max:300',
        ];
        if ($this->method() == 'POST') {
            
            $arr['email'] = ['email','unique:users'];
            $arr['password'] = ['required', 'regex:/^\S*$/u', 'string', 'max:255'];
        }
        return $arr;
    }

    // Validation Messages
    public function messages()
    {
        return [
            'required' => 'Kolom :attribute tidak boleh kosong',
            'exists' => 'Kolom :attribute tidak valid',
            'email' => 'Email tidak valid',
            'unique' => ':attribute sudah digunakan',
            'numeric' => 'Kolom :attribute hanya boleh berisi angka',
            'max' => 'Kolom :attribute maksimal :max karakter',
            'photo.max' => 'Ukuran maksimal 4Mb',
            'file' => 'Photo tidak valid',
            'mimes' => 'Photo harus berekstensi JPG, JPEG, PNG',
            'min' => 'Kolom :attribute minimal 10 karakter',
            'integer' => 'Kolom :attribute hanya boleh berisi nomor',
            'regex' => 'Kolom :attribute tidak boleh mengandung spasi',
        ];
    }

    // Custom Attribute
    public function attributes()
    {
        return [];
    }
}
