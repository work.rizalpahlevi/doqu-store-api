<?php

// namespace App\Http\Requests;

// use App\Http\Requests\CustomFormRequest;

// class <Nama Request> extends CustomFormRequest
// {
//     /**
//      * Determine if the user is authorized to make this request.
//      *
//      * @return bool
//      */
//     public function authorize()
//     {
//         return true;
//     }

//     /**
//      * Get the validation rules that apply to the request.
//      *
//      * @return array
//      */
//     public function rules()
//     {
        // BAGIAN $arr INI DESUAIKAN DENGAN VALIDASI DAN KOLOM SETIAP TABLE DI DB NYA
//         $arr = [
//             'user_id' => 'required|numeric',
//             'date_time' => 'nullable|max:10',
//             'modul' => 'nullable|max:255',
//             'info' => 'nullable|max:500',
//         ];
//         return $arr;
//     }

//     // messages
//     public function messages()
//     {
//         return [
//             'required' => 'Kolom :attribute tidak boleh kosong',
//             'exists' => 'Kolom :attribute tidak valid',
//             'email' => 'Email tidak valid',
//             'unique' => ':attribute sudah digunakan',
//             'numeric' => 'Kolom :attribute hanya boleh berisi angka',
//             'max' => 'Kolom :attribute maksimal :max karakter',
//             'photo.max' => 'Ukuran maksimal 4Mb',
//             'file' => 'Photo tidak valid',
//             'mimes' => 'Photo harus berekstensi JPG, JPEG, PNG',
//             'min' => 'Kolom :attribute minimal 10 karakter',
//             'integer' => 'Kolom :attribute hanya boleh berisi nomor'
//         ];
//     }

//     // attributes
//     public function attributes()
//     {
//         return [
//         ];
//     }
// }
