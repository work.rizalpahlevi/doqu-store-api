<?php

namespace App\Http\Controllers;

use App\Repositories\ProductGameRepository;
use App\Serializers\DataSerializer;
use App\Transformers\ProductGameTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ProductGameController extends Controller
{
    protected $repository;
    protected $manager;

    public function __construct(ProductGameRepository $repository, Manager $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->manager->setSerializer(new DataSerializer());
    }


    public function show(Request $request)
    {
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $items = $this->repository->getAll($request->input('page_size') == null ? 10 : $request->input('page_size'), $queryParams, $request);

        $resource = new Collection($items['items'], new ProductGameTransformer(), 'data');

        $resource->setPaginator($items['paginator_adapter']);
        return $this->manager->createData($resource)->toArray();
    }

    public function getById($id)
    {
        $item = $this->repository->getById($id);
        if ($item) {
            $resource = new Item($item, new ProductGameTransformer(), 'data');
            return $this->manager->createData($resource)->toArray();
        }
        return response()->make('', 204);
    }
}
