<?php

// namespace App\Http\Controllers;

// use App\Repositories\ <Nama Repository>;
// use App\Http\Requests\ <Nama Request>;
// use App\Transformers\ <Nama Transformer>;
// use App\Serializers\DataSerializer;
// use Illuminate\Http\Request;
// use League\Fractal\Manager;
// use League\Fractal\Resource\Collection;
// use League\Fractal\Resource\Item;

// class <Nama Controller> extends Controller
// {
//     protected $repository;
//     protected $manager;

//     public function __construct(<Nama Repository> $repository, Manager $manager)
//     {
//         $this->repository = $repository;
//         $this->manager = $manager;
//         $this->manager->setSerializer(new DataSerializer());
//     }


//     public function show(Request $request)
//     {
//         $queryParams = array_diff_key($_GET, array_flip(['page']));
//         $items = $this->repository->getAll($request->input('page_size') == null ? 10 : $request->input('page_size') , $queryParams, $request);

//         $resource = new Collection($items['items'], new <Nama Transformer>(), 'data');

//         $resource->setPaginator($items['paginator_adapter']);
//         return $this->manager->createData($resource)->toArray();
//     }

//     public function getById($id)
//     {
//         $item = $this->repository->getById($id);
//         if ($item) {
//             $resource = new Item($item, new <Nama Transformer>(), 'data');
//             return $this->manager->createData($resource)->toArray();
//         }
//         return response()->make('', 204);
//     }

//     public function store(<Nama Request> $request)
//     {
//         
//         $item = $this->repository->insert($request);

//         $resource = new Item($item, new <Nama Transformer>(), 'data');
//         $response = [
//             'title' => 'Berhasil',
//             'message' => 'Berhasil menambah master <Nama Master>',
//             'item' => $this->manager->createData($resource)->toArray()
//         ];
//         return $response;
//     }

//     public function update($id, <Nama Request> $request)
//     {
//         
//         $item = $this->repository->update($id, $request);

//         if ($item) {
//             $resource = new Item($item, new MasterAgencyTypeTransformer(), 'data');
//             $response = [
//                 'title' => 'Berhasil',
//                 'message' => 'Berhasil mengubah master <Nama master>',
//                 'item' => $this->manager->createData($resource)->toArray()
//             ];
//             return $response;
//         }
//         return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
//     }

//     public function destroy($id)
//     {
//         $item = $this->repository->delete($id);
//         if ($item) {
//             $resource = new Item($item, new <Nama Transformer>(), 'data');
//             $response = [
//                 'title' => 'Berhasil',
//                 'message' => 'Berhasil menghapus master <Nama Master>',
//                 'item' => $this->manager->createData($resource)->toArray()
//             ];
//             return $response;
//         }
//         return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
//     }
// }
