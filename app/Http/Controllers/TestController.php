<?php

namespace App\Http\Controllers;

use App\Models\OdkSms;
use App\Repositories\AccessListRepository;
use App\Serializers\DataSerializer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class TestController extends Controller
{

  protected $repository;
  protected $manager;

  public function __construct(AccessListRepository $repository, Manager $manager)
  {
    $this->repository = $repository;
    $this->manager = $manager;
    $this->manager->setSerializer(new DataSerializer());
  }

  public function test(Request $request)
  {
    // $odk_briefing = OdkBriefing::take(10)->get();
    // $odk_ceklap = OdkCekLap::take(10)->get();
    // $odk_ep = OdkEP::take(10)->get();
    // $odk_ep_tps = OdkEpTps::take(10)->get(); // =========================== error message "message": "Malformed UTF-8 characters, possibly incorrectly encoded",
    // $odk_form_deleted = OdkFormDeleted::take(10)->get();
    // $odk_form_ssistm = OdkFormSsistmpDeleted::take(10)->get();
    // $odk_hasil = OdkHasil::take(10)->get();
    // $odk_import_log = OdkImportLog::take(10)->get();
    // $odk_pe2 = OdkPe2::take(10)->get();
    // $odkpengisianjabarcore = OdkPengisianjabarCore::take(10)->get();
    // $odkqc = OdkQc::take(10)->get();
    // $odk_sapapagi = OdkSapaPagi::take(10)->get();
    // $odk_sapapilpres = OdkSapaPilpres::take(10)->get();
    $odk_sms = OdkSms::take(10)->get();

    $odk = [
      // 'odk_briefing' => $odk_briefing,
      // 'odk_ceklap' => $odk_ceklap,
      // 'odk_ep' => $odk_ep,
      // 'odk_ep_tps' => $odk_ep_tps,
      // 'odk_form_deleted' => $odk_form_deleted,
      // 'odk_form_ssistm' => $odk_form_ssistm,
      // 'odk_hasil' => $odk_hasil,
      // 'odk_import_log' => $odk_import_log,
      // 'odk_pe2' => $odk_pe2,
      // 'odkpengisianjabarcore' => $odkpengisianjabarcore,
      // 'odkqc' => $odkqc,
      // 'odk_sapapagi' => $odk_sapapagi,
      // 'odk_sapapilpres' => $odk_sapapilpres,
      'odk_sms' => $odk_sms,
    ];

    return $odk;
  }

  public function insertMasterTable(Request $request)
  {
    // $item = new MasterTable;
    // $item->name = "ODK EP TPS";
    // $item->key_name = "odk_ep_tps";
    // $item->save();
    return "You can't find anything here";
  }

  public function testInsertS3(Request $request)
  {
    $dt = Carbon::now();
    $year = Carbon::parse($dt)->format('Y');
    $month = Carbon::parse($dt)->format('m');
    $uuid = Uuid::uuid4();

    $file = $request->file('file'); 
    $fileName = $file->getClientOriginalName();
    $filePath = '/quickcount/test/'. $year . '/' . $month . '/' . $uuid . "/" . $fileName;
    // $filePath = env("AWS_TENANT_DIR") . '/' . $input->header('name') . '/medicines/' . $year . '/' . $month . '/' . $fileName;
    // dd($filePath);
    return Storage::disk('s3')->put($filePath, file_get_contents($request->file));

  }
}
