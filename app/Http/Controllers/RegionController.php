<?php

namespace App\Http\Controllers;

use League\Fractal\Manager;
use App\Serializers\DataSerializer;
use App\Models\District;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Models\Village;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

/** @OA\Get(
 *       path="/api/region/provinces",
 *       tags={"Region"},
 *       operationId="getProvinces",
 *       summary="provinces",
 *       description="Get Data Provinces",
 *       @OA\Parameter(
 *            name="search",
 *            in="query",
 *            description="Search",
 *            required=false,
 *            @OA\Schema(type="string"),
 *       ),
 *       security={{"bearerAuth":{}}},
 *       @OA\Response(
 *           response="200",
 *           description="Ok",
 *           @OA\JsonContent
 *           (example={
 *          }),
 *      ),
 *  )
 * 
 * @OA\Get(
 *       path="/api/region/districts/{province_id}",
 *       tags={"Region"},
 *       operationId="getDistricts",
 *       summary="districts",
 *       description="Get Data District By Province Id",
 *       @OA\Parameter(
 *            name="province_id",
 *            in="path",
 *            description="ID Provinsi",
 *            required=true,
 *            @OA\Schema(type="integer"),
 *       ),
 *       @OA\Parameter(
 *            name="search",
 *            in="query",
 *            description="Search",
 *            required=false,
 *            @OA\Schema(type="string"),
 *       ),
 *       security={{"bearerAuth":{}}},
 *       @OA\Response(
 *           response="200",
 *           description="Ok",
 *           @OA\JsonContent
 *           (example={
 *          }),
 *      ),
 *  )
 * 
 * @OA\Get(
 *       path="/api/region/subdistricts/{district_id}",
 *       tags={"Region"},
 *       operationId="getSubdistricts",
 *       summary="subdistricts",
 *       description="Get Data Sub District By District Id",
 *       @OA\Parameter(
 *            name="district_id",
 *            in="path",
 *            description="ID Distrik",
 *            required=true,
 *            @OA\Schema(type="integer"),
 *       ),
 *       @OA\Parameter(
 *            name="search",
 *            in="query",
 *            description="Search",
 *            required=false,
 *            @OA\Schema(type="string"),
 *       ),
 *       security={{"bearerAuth":{}}},
 *       @OA\Response(
 *           response="200",
 *           description="Ok",
 *           @OA\JsonContent
 *           (example={
 *          }),
 *      ),
 *  )
 * 
 * @OA\Get(
 *       path="/api/region/villages/{subdistrict_id}",
 *       tags={"Region"},
 *       operationId="getVillages",
 *       summary="villages",
 *       description="Get Data Village By subdistrict Id",
 *       @OA\Parameter(
 *            name="subdistrict_id",
 *            in="path",
 *            description="ID Subdistrict",
 *            required=true,
 *            @OA\Schema(type="integer"),
 *       ),
 *       @OA\Parameter(
 *            name="search",
 *            in="query",
 *            description="Search",
 *            required=false,
 *            @OA\Schema(type="string"),
 *       ),
 *       security={{"bearerAuth":{}}},
 *       @OA\Response(
 *           response="200",
 *           description="Ok",
 *           @OA\JsonContent
 *           (example={
 *          }),
 *      ),
 *  )
 */

class RegionController extends Controller
{
    protected $repository;
    protected $manager;

    public function __construct(CategoryRepository $repository, Manager $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->manager->setSerializer(new DataSerializer());
    }


    public function province(Request $request)
    {
        $data = new Province();

        if ($request->has('search') && $request->search != null) {
            $data = $data->where('name', 'like', "%$request->search%");
        }

        $data = $data->get();

        if (!$data) {
            return response()->make('', 204);
        }

        $datas = [
            "data" => $data
        ];

        // response
        return response()->json($datas);
    }

    public function district($province_id, Request $request)
    {
        $data = District::where('province_id', $province_id);

        if ($request->has('search') && $request->search != null) {
            $data = $data->where('name', 'like', "%$request->search%");
        }

        $data = $data->get();

        if (!$data) {
            return response()->make('', 204);
        }


        $datas = [
            "data" => $data
        ];
        // response
        return response()->json($datas);
    }

    public function subdistrict($district_id, Request $request)
    {
        $data = Subdistrict::where('district_id', $district_id);

        if ($request->has('search') && $request->search != null) {
            $data = $data->where('name', 'like', "%$request->search%");
        }

        $data = $data->get();

        if (!$data) {
            return response()->make('', 204);
        }

        $datas = [
            "data" => $data
        ];
        // response
        return response()->json($datas);
    }

    public function village($subdistrict_id, Request $request)
    {
        $data = Village::where('subdistrict_id', $subdistrict_id);

        if ($request->has('search') && $request->search != null) {
            $data = $data->where('name', 'like', "%$request->search%");
        }

        $data = $data->get();

        if (!$data) {
            return response()->make('', 204);
        }

        $datas = [
            "data" => $data
        ];
        // response
        return response()->json($datas);
    }
}
