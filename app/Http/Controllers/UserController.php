<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Serializers\DataSerializer;
use App\Transformers\UserTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Http\Requests\UserRequest;

/**
 *    @OA\Get(
 *       path="/api/users",
 *       tags={"User"},
 *       operationId="Users",
 *       summary="Pengguna",
 *       description="Mengambil Data Pengguna",
 *       @OA\Response(
 *           response="200",
 *           description="Ok",
 *           @OA\JsonContent
 *           (example={
 *               "success": true,
 *               "message": "Berhasil mengambil Pengguna",
 *               "data": {
 *                   {
 *           "id": 3,
 *           "name": "Loper",
 *           "email": "loper@wellmagic.id",
 *           "role_id": 3,
 *           "role_name": "Loper",
 *           "role_object_id": "loper",
 *           "phone": "62123456789",
 *           "mobile_phone": "62123456789",
 *           "description": "",
 *           "photo": "https://is3.cloudhost.id/medhop-prod-bucket//users/default-user.svg",
 *           "created_at": "2023-11-29T12:38:20.000000Z",
 *           "created_by": 0,
 *           "created_by_name": "",
 *           "updated_at": "2023-11-29T12:38:20.000000Z",
 *           "updated_by": 0,
 *           "updated_by_name": "",
 *           "deleted_at": null
 *       }
 *              }
 *          }),
 *      ),
 *  )
 * 
 * @OA\Post(
 *     path="/oauth/token",
 *     tags={"OAuth"},
 *     operationId="getToken",
 *     summary="Get Access Token",
 *     description="Request an access token using the password grant type.",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="username", type="string", description="The username (email) of the user."),
 *             @OA\Property(property="password", default="password" ,type="string", format="password", description="The user's password."),
 *             @OA\Property(property="grant_type", type="string", example="password", description="The grant type (password)."),
 *             @OA\Property(property="client_id", type="string", description="The client ID."),
 *             @OA\Property(property="client_secret", type="string", format="password", description="The client secret."),
 *             @OA\Property(property="scope", default="*" ,type="string", description="The scope of the access request."),
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Access token successfully obtained",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="token_type", type="string", example="Bearer"),
 *             @OA\Property(property="expires_in", type="integer", example=3600),
 *             @OA\Property(property="access_token", type="string"),
 *             @OA\Property(property="refresh_token", type="string"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthorized",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="error", type="string", example="invalid_credentials"),
 *             @OA\Property(property="error_description", type="string", example="The user credentials were incorrect."),
 *         ),
 *     ),
 * )
 * 
 * @OA\Post(
 *     path="/api/register",
 *     tags={"User"},
 *     operationId="registerUser",
 *     summary="Register a new user",
 *     description="Create a new user account with the provided information.",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="name", type="string", description="The name of the user."),
 *             @OA\Property(property="email", type="string", format="email", description="The email address of the user."),
 *             @OA\Property(property="role_id", type="integer", description="The role ID of the user."),
 *             @OA\Property(property="phone", type="string", description="The phone number of the user."),
 *             @OA\Property(property="description", type="string", description="The description of the user."),
 *             @OA\Property(property="password", type="string", format="password", description="The password for the user."),
 *         )
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="User registered successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="User registered successfully"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response="422",
 *         description="Validation error",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="Validation error"),
 *             @OA\Property(property="errors", type="object"),
 *         ),
 *     ),
 * )
 */

class UserController extends Controller
{
    protected $repository;
    protected $manager;


    public function __construct(UserRepository $repository, Manager $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->manager->setSerializer(new DataSerializer());
    }

    public function show(Request $request)
    {
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $items = $this->repository->getAll($request->input('page_size') == null ? 10 : $request->input('page_size'), $queryParams, $request);
        $resource = new Collection($items['items'], new UserTransformer(), 'data');
        $resource->setPaginator($items['paginator_adapter']);
        return $this->manager->createData($resource)->toArray();
    }

    public function showAllUserRole(Request $request)
    {
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $items = $this->repository->getAllUserRole($request->input('page_size') == null ? 10 : $request->input('page_size'), $queryParams, $request);
        $resource = new Collection($items['items'], new UserTransformer(), 'data');
        $resource->setPaginator($items['paginator_adapter']);
        return $this->manager->createData($resource)->toArray();
    }

    public function me()
    {
        $item = $this->repository->me();
        $response = new Item($item, new UserTransformer(), 'data');
        return $this->manager->createData($response)->toArray();
    }

    public function getById($id)
    {
        $item = $this->repository->getById($id);
        $response = new Item($item, new UserTransformer(), 'data');
        return $this->manager->createData($response)->toArray();
    }

    // public function getById($id)
    // {
    //     $item = $this->repository->getById($id);
    //     $meta = $this->repository->getDetailUserMeta($id);
    //     if ($item) {
    //         $resource = new Item($item, new UserTransformer(), 'data');
    //         $resource->setMeta($meta);
    //         return $this->manager->createData($resource)->toArray();
    //     }
    //     return response()->make('', 204);
    // }

    public function store(UserRequest $request)
    {
        $item = $this->repository->insert($request);
        $resource = new Item($item, new UserTransformer(), 'data');
        $response = [
            'title' => 'Berhasil',
            'message' => 'Berhasil menambah master user',
            'item' => $this->manager->createData($resource)->toArray()
        ];
        return $response;
    }

    public function update($id, UserRequest $request)
    {
        $item = $this->repository->update($id, $request);

        if ($item) {
            $resource = new Item($item, new UserTransformer(), 'data');
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil mengubah master user',
                'item' => $this->manager->createData($resource)->toArray()
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        if ($item) {
            $resource = new Item($item, new UserTransformer(), 'data');
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil menghapus master user',
                'item' => $this->manager->createData($resource)->toArray()
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    public function setUserOn($id)
    {
        $item = $this->repository->setUserOn($id);
        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil On',
                'item' => $item
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    public function setUserOff($id)
    {
        $item = $this->repository->setUserOff($id);
        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil Off',
                'item' => $item
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    // Change Password
    public function changePassword(ChangePasswordRequest $request, $id)
    {
        $item = $this->repository->changePassword($request, $id);
        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil Mengedit Password',
                'item' => $item
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    public function importExcel(Request $request)
    {
        // import excel validation
        $this->validate(
            $request,
            [
                'file' => 'required|mimes:csv,txt,xlsx|max:4096'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'mimes' => ':attribute harus berformat csv atau xlsx',
                'max' => 'Ukuran maksimal :attribute 4Mb'
            ],
            [
                'file' => 'File'
            ]
        );

        $item = $this->repository->importExcel($request);
        return $item;
    }

    public function reset(Request $request)
    {
        $item = $this->repository->reset($request);
        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil mereset master TPS',
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }
}
