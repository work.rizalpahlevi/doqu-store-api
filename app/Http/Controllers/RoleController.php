<?php

namespace App\Http\Controllers;

use App\Repositories\RoleRepository;
use App\Serializers\DataSerializer;
use App\Transformers\RoleTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class RoleController extends Controller
{

    protected $repository;
    protected $manager;

    public function __construct(RoleRepository $repository, Manager $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->manager->setSerializer(new DataSerializer());
    }

    protected function validation(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255'
        ], [
            'required' => 'Kolom wajib diisi',
            'name.max' => 'Kolom Nama tidak boleh melebihi 255 karakter',
            'description.max' => 'Kolom Deskripsi tidak boleh melebihi 255 karakter'
        ]);
    }

    public function show(Request $request)
    {
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $items = $this->repository->getAll($request->input('page_size') == null ? 10 : $request->input('page_size') , $queryParams, $request);
        $resource = new Collection($items['items'], new RoleTransformer(), 'data');
        $resource->setPaginator($items['paginator_adapter']);
        return $this->manager->createData($resource)->toArray();
    }

    public function getById($id)
    {
        $item = $this->repository->getById($id);
        $meta = $this->repository->getDetailRoleMeta($id);
        if ($item) {
            $resource = new Item($item, new RoleTransformer(), 'data');
            $resource->setMeta($meta);
            return $this->manager->createData($resource)->toArray();
        }
        return response()->make('', 204);
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $item = $this->repository->insert($request);
        $response = [
            'title' => 'Berhasil',
            'message' => 'Berhasil menambah jabatan',
            'item' => $this->response->item($item, new RoleTransformer())
        ];
        return $response;
    }

    public function update($id, Request $request)
    {
        $this->validation($request);
        $item = $this->repository->update($id, $request);

        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil mengubah jabatan',
                'item' => $this->response->item($item, new RoleTransformer())
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }

    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        if ($item) {
            $response = [
                'title' => 'Berhasil',
                'message' => 'Berhasil menghapus jabatan',
                'item' => $this->response->item($item, new RoleTransformer())
            ];
            return $response;
        }
        return response()->json((object) ['status_code' => 404, 'message' => 'Tidak ditemukan.'], 404);
    }
}
