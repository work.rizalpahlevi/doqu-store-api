<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductGameController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\RegionController;
use Laravel\Passport\Http\Controllers\AccessTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [UserController::class, 'store']);

Route::post('oauth/token', [AccessTokenController::class, 'issueToken']);

Route::middleware('authapi:api')->group(function () {
    /*
    |--------------------------------------------
    | MASTER
    |--------------------------------------------
    */

    Route::middleware('logrequest')->group(function () {
        // Account
        Route::put('account/change-password/{id}', [AuthController::class, 'changePassword']);

        //Roles
        Route::get('roles', [RoleController::class, 'show']);
        Route::get('roles/{id}', [RoleController::class, 'getById']);
        Route::post('roles', [RoleController::class, 'store']);
        Route::put('roles/{id}', [RoleController::class, 'update']);
        Route::delete('roles/{id}', [RoleController::class, 'destroy']);

        //Users
        Route::get('users', [UserController::class, 'show']);
        Route::get('users', [UserController::class, 'show']);
        Route::get('users-all-role', [UserController::class, 'showAllUserRole']);
        Route::get('users/me', [UserController::class, 'me']);
        Route::get('users/{id}', [UserController::class, 'getById']);
        
        Route::post('users/import', [UserController::class, 'importExcel']);
        Route::put('users/{id}', [UserController::class, 'update']);
        Route::delete('users/{id}', [UserController::class, 'destroy']);

        // Region
        Route::get('region/provinces', [RegionController::class, 'province']);
        Route::get('region/districts/{province_id}', [RegionController::class, 'district']);
        Route::get('region/subdistricts/{district_id}', [RegionController::class, 'subdistrict']);
        Route::get('region/villages/{subdistrict_id}', [RegionController::class, 'village']);

        // Product Game
        Route::get('products', [ProductGameController::class, 'show']);
        Route::get('products/{id}', [ProductGameController::class, 'getById']);

        // Promotion
        Route::get('promotions', [PromotionController::class, 'show']);
        Route::get('promotions/{id}', [PromotionController::class, 'getById']);
    });
});
