<?php

namespace Database\Seeders;

use App\Models\Promotion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/promotions.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Promotion::create(array(
                "name" => $obj->name,
                "description" => $obj->description,
                "banner" => $obj->banner
            ));
        }
    }
}
