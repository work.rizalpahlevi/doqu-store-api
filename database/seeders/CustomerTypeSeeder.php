<?php

namespace Database\Seeders;

use App\Models\MasterCustomerType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CustomerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/master_customer_type.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterCustomerType::create(array(
                'name' => $obj->name,
                'description' => $obj->description
            ));
        }
    }
}
