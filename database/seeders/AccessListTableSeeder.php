<?php

namespace Database\Seeders;

use App\Models\AccessList;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AccessListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(storage_path('data-multitenant/accesslists.json'));
        $data = json_decode($json);
        DB::unprepared('SET IDENTITY_INSERT access_lists ON');
        foreach ($data as $obj) {
            AccessList::create(array(
                'id' => $obj->id,
                'code' => $obj->code,
                'parent_id' => $obj->parent_id,
                'name' => $obj->name,
                'is_active' => $obj->is_active,
            ));
        }
        DB::unprepared('SET IDENTITY_INSERT access_lists OFF');
    }
}
