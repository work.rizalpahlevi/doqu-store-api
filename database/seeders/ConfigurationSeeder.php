<?php

namespace Database\Seeders;

use App\Helpers\DateHelper;
use App\Models\Configuration;
use Illuminate\Database\Seeder;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = [
            "mode" => 1,
            "work_time_in" => "06:00:00",
            "work_time_out" => "18:00:00"
        ];

        // Menggunakan eloquent untuk menyimpan data
        Configuration::create($config);
        
    }
}
