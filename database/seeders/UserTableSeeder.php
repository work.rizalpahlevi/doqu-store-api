<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::factory()->times(5)->create();
        $json = File::get('database/data/users.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            User::create(array(
                'role_id' => $obj->role_id,
                'name' => $obj->name,
                'email' => $obj->email,
                'password' => Hash::make($obj->password),
                'phone' => $obj->phone,
                'mobile_phone' => $obj->mobile_phone,
                'description' => $obj->description,
                'photo' => $obj->photo,
            ));
        }
    }
}
