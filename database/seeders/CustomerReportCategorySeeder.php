<?php

namespace Database\Seeders;

use App\Helpers\DateHelper;
use App\Models\CategoryCustomerReport;
use App\Models\Configuration;
use Illuminate\Database\Seeder;

class CustomerReportCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = [
            [
                "parent" => 0,
                "name" => "Produk",

            ],
            [
                "parent" => 1,
                "name" => "Produk Kurang",

            ],
            [
                "parent" => 1,
                "name" => "Produk Rusak",
                
            ],
        ];

        // Menggunakan eloquent untuk menyimpan data
        CategoryCustomerReport::create($config);
    }
}
