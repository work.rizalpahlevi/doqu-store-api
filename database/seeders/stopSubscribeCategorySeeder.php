<?php

namespace Database\Seeders;

use App\Models\StopSubscribeCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class stopSubscribeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                "name" => "Batal",
                "description" => "Batal"
            ],
            [
                "name" => "Pindah ke media digital",
                "description" => "Pindah ke media digital"
            ],
            [
                "name" => "Mengurangi Paket",
                "description" => "Mengurangi Paket"
            ],
        ];

        // Menggunakan eloquent untuk menyimpan data
        foreach ($categories as $category) {
            StopSubscribeCategory::create($category);
        }
    }
}
