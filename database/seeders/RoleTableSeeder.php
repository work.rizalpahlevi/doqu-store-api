<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/roles.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            Role::create(array(
                'object_id' => $obj->object_id,
                'name' => $obj->name,
                'description' => $obj->description
            ));
        }
    }
}
