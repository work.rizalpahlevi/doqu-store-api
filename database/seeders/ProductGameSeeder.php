<?php

namespace Database\Seeders;

use App\Models\ProductGame;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class ProductGameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/game-product.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            ProductGame::create(array(
                "name" => $obj->name,
                "description" => $obj->description,
                "icon" => $obj->icon
            ));
        }
    }
}
