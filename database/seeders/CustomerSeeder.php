<?php

namespace Database\Seeders;

use App\Models\MasterCustomer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::factory()->times(5)->create();
        $json = File::get('database/data/master_customer.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterCustomer::create(array(
                'name' => $obj->name,
                'address' => $obj->address,
                'province_id' => $obj->province_id,
                'district_id' => $obj->district_id,
                'subdistrict_id' => $obj->subdistrict_id,
                'village_id' => $obj->village_id,
                'longitude' => $obj->longitude,
                'latitude' => $obj->latitude,
                'phone' => $obj->phone,
                'mobile_phone' => $obj->mobile_phone,
                'email' => $obj->email,
                'subcription_date_start' => $obj->subcription_date_start,
                'agency_id' => $obj->agency_id,
                'service_point_id' => $obj->service_point_id,
                'customer_type_id' => $obj->customer_type_id,
                'customer_status_id' => $obj->customer_status_id,
            ));
        }
    }
}
