<?php

namespace Database\Seeders;

use App\Models\MasterProduct;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class MasterProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/master_product.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterProduct::create(array(
                'name' => $obj->name,
                'description' => $obj->description
            ));
        }
    }
}
