<?php

namespace Database\Seeders;

use App\Helpers\DateHelper;
use App\Models\Delivery;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deliveries = [
            [
                "courier_id" => 1,
                "date" => Carbon::now('GMT+7')->format('Y-m-d'),
                "delivery_date_start" => DateHelper::getCurrentTime(),
                "delivery_date_end" => null,
                "workstate_id" => 1
            ],
            [
                "courier_id" => 2,
                "date" => Carbon::now('GMT+7')->format('Y-m-d'),
                "delivery_date_start" => DateHelper::getCurrentTime(),
                "delivery_date_end" => null,
                "workstate_id" => 1
            ],
            [
                "courier_id" => 3,
                "date" => Carbon::now('GMT+7')->format('Y-m-d'),
                "delivery_date_start" => DateHelper::getCurrentTime(),
                "delivery_date_end" => null,
                "workstate_id" => 1
            ],
        ];

        // Menggunakan eloquent untuk menyimpan data
        foreach ($deliveries as $delivery) {
            Delivery::create($delivery);
        }
    }
}
