<?php

namespace Database\Seeders;

use App\Models\AccessListRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class AccessListRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(storage_path('data-multitenant/accesslistroles.json'));
        $data = json_decode($json);
        foreach ($data as $obj) {
            AccessListRole::create(array(
                'access_list_id' => $obj->access_list_id,
                'role_id' => $obj->role_id
            ));
        }
    }
}
