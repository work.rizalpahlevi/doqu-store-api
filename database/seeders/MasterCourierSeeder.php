<?php

namespace Database\Seeders;

use App\Models\MasterCourier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class MasterCourierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/master_courier.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterCourier::create(array(
                "agency_id" => $obj->agency_id,
                "user_id" => $obj->user_id,
                "name" => $obj->name,
                "address" => $obj->address,
                "province_id" => $obj->province_id,
                "city_id" => $obj->city_id,
                "district_id" => $obj->district_id,
                "subdistrict_id" => $obj->subdistrict_id,
                "phone" => $obj->phone,
                "mobile_phone" => $obj->mobile_phone,
            ));
        }
    }
}
