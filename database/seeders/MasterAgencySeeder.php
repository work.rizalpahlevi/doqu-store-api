<?php

namespace Database\Seeders;

use App\Models\MasterAgency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class MasterAgencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/master_agency.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterAgency::create(array(
                "name" => $obj->name,
                "agent_type_id" => $obj->agent_type_id,
                "address" => $obj->address,
                "province_id" => $obj->province_id,
                "district_id" => $obj->district_id,
                "subdistrict_id" => $obj->subdistrict_id,
                "village_id" => $obj->village_id,
                "longitude" => $obj->longitude,
                "latitude" => $obj->latitude,
                "telephone" => $obj->telephone,
                "mobile_phone" => $obj->mobile_phone,
                "drop_point_id" => $obj->drop_point_id
            ));
        }
    }
}
