<?php

namespace Database\Seeders;

use App\Models\CustomerDeliveryItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CustomerDeliveryItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/customer_delivery_item.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            CustomerDeliveryItem::create(array(
                "delivery_id" => $obj->delivery_id,
                "customer_delivery_id" => $obj->customer_delivery_id,
                "product_id" => $obj->product_id,
                "product_name" => $obj->product_name,
                "product_quantity" => $obj->product_quantity
            ));
        }
    }
}
