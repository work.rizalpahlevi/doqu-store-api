<?php

namespace Database\Seeders;

use App\Models\MasterDeliveryWorkstate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DeliveryWorkstateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::factory()->times(5)->create();
        $json = File::get('database/data/master_delivery_workstate.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterDeliveryWorkstate::create(array(
                'name' => $obj->name,
                'description' => $obj->description
            ));
        }
    }
}
