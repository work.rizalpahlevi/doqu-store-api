<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ProductGameSeeder::class);
        $this->call(PromotionSeeder::class);
        // $this->call(MasterAgencySeeder::class);
        // $this->call(DeliveryWorkstateSeeder::class);
        // $this->call(MasterCourierSeeder::class);
        // $this->call(MasterProductSeeder::class);
        // $this->call(MasterAgencySeeder::class);
        // $this->call(DeliverySeeder::class);
        // $this->call(DeliveryDetailSeeder::class);
        // $this->call(CustomerStatusSeeder::class);
        // $this->call(CustomerTypeSeeder::class);
        // $this->call(CustomerDeliveriesSeeder::class);
        // $this->call(CustomerDeliveryItemsSeeder::class);
        // $this->call(ConfigurationSeeder::class);
        // $this->call(CustomerReportCategorySeeder::class);
    }
}
