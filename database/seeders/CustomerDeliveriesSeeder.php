<?php

namespace Database\Seeders;

use App\Models\CustomerDelivery;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CustomerDeliveriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/customer_delivery.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            CustomerDelivery::create(array(
                "delivery_id" => $obj->delivery_id,
                "customer_id" => $obj->customer_id,
                "customer_name" => $obj->customer_name,
                "delivery_address" => $obj->delivery_address,
                "longitude" => $obj->longitude,
                "latitude" => $obj->latitude,
                "phone" => $obj->phone,
                "mobile_phone" => $obj->mobile_phone,
                "agency_id" => $obj->agency_id,
                "courier_id" => $obj->courier_id,
                "notes" => $obj->notes,
                "receiver_picture" => $obj->receiver_picture,
                "receiver_name" => $obj->receiver_name,
                "date_time_start" => $obj->date_time_start,
                "date_time_end" => $obj->date_time_end,
                "total_distance" => $obj->total_distance,
                "total_time" => $obj->total_time,
                "workstate_id" => $obj->workstate_id
            ));
        }
    }
}
