<?php

namespace Database\Seeders;

use App\Models\MasterCustomerStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CustomerStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get('database/data/master_customer_status.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            MasterCustomerStatus::create(array(
                'name' => $obj->name,
                'description' => $obj->description
            ));
        }
    }
}
