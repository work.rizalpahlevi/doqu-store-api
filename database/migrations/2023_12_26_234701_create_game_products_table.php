<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('description',255)->nullable();
            $table->string('icon',255)->nullable();
            $table->bigInteger('created_by')->nullable()->default(0);
            $table->bigInteger('updated_by')->nullable()->default(0);
            $table->bigInteger('deleted_by')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_products');
    }
};
