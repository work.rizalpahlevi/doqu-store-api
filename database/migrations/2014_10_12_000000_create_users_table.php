<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->nullable()->default(0);
            $table->string('name', 255);
            $table->string('email');
            $table->string('password', 64);
            $table->string('phone', 30);
            $table->string('mobile_phone', 30);
            $table->text('description')->nullable();
            $table->string('photo')->nullable();
            $table->bigInteger('created_by')->nullable()->default(0);
            $table->bigInteger('updated_by')->nullable()->default(0);
            $table->bigInteger('deleted_by')->nullable()->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
