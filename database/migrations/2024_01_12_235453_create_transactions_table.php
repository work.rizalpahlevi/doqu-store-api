<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('product_item_id');
            $table->integer('quantity');
            $table->integer('amount');
            $table->string('user_id')->nullable();
            $table->string('zone_id')->nullable();
            $table->integer('worstate_id');
            $table->text('notes')->nullable();
            $table->bigInteger('created_by')->nullable()->default(0);
            $table->bigInteger('updated_by')->nullable()->default(0);
            $table->bigInteger('deleted_by')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
