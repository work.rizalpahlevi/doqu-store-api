<?php

return [

    'paths' => ['api/*', 'sanctum/csrf-cookie','api'],
    'allowed_methods' => ['*'],
    'allowed_origins' => ['https://looper-dev.wellmagic.id','https://looper-dev.wellmagic.id/#','http://localhost:8080'], // Add your allowed origin here 
    'allowed_headers' => ['Content-Type', 'X-Requested-With', 'Authorization'],
    'exposed_headers' => [],
    'max_age' => 0,
    'supports_credentials' => false,
];
